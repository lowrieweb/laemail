﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LAeMail.Infrastructure;
using System.IO;
using System.Windows.Threading;
using System.Windows;
using System.Threading;
using System.Net.Mail;
using System.Net;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Xml;

namespace LAeMail
{
    public class UpdateManager : PropertyHelpersBase 
    {
        #region Private
        private Version currentVersion = Assembly.GetExecutingAssembly().GetName().Version;
        private Version newVersion = null;
        private Dispatcher dispatcher = null;
        #endregion

        #region Properties
        public bool UpdateAvailable
        {
            get { return Get(() => UpdateAvailable, false); }
            set { Set(() => UpdateAvailable, value); }
        }

        public String CurrentVersion
        {
            get { return Get(() => CurrentVersion, currentVersion.ToString()); }
            set { Set(() => CurrentVersion, value); }
        }

        public String NewVersion
        {
            get { return Get(() => NewVersion); }
            set { Set(() => NewVersion, value); }
        }

        public String UpdateLink
        {
            get { return Get(() => UpdateLink); }
            set { Set(() => UpdateLink, value); }
        }
        #endregion

        public UpdateManager(Action<bool> onDone, Action<LogLevel, string> onNotify)
        {
            this.dispatcher = Application.Current.Dispatcher;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Properties.Settings.Default.UpdateManager);
                request.BeginGetResponse(new AsyncCallback((result) => 
                {
                    try
                    {
                        using (HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(result))
                        {
                            using (XmlReader reader = new XmlTextReader(response.GetResponseStream()))
                            {
                                int found = 0;

                                while (reader.Read())
                                {
                                    if (reader.NodeType == XmlNodeType.Element && reader.LocalName.ToLower() == "url")
                                    {
                                        reader.Read();
                                        UpdateLink = reader.Value;
                                        found++;
                                    }
                                    else if (reader.NodeType == XmlNodeType.Element && reader.LocalName.ToLower() == "currentversion")
                                    {
                                        reader.Read();
                                        newVersion = new Version(reader.Value);
                                        NewVersion = newVersion.ToString();
                                        found++;
                                    }

                                    if (found == 2)
                                    {
                                        if (dispatcher != null && onDone != null)
                                            dispatcher.BeginInvoke(onDone, new object[] { newVersion.CompareTo(currentVersion) > 0 });

                                        return;
                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        if (dispatcher != null && onNotify != null)
                            dispatcher.BeginInvoke(onNotify, new object[] { LogLevel.Warning, Properties.Resources.WARNING_UpdateManager });
                    }
                }), null); 
            }
            catch 
            {
                if (dispatcher != null && onNotify != null)
                    dispatcher.BeginInvoke(onNotify, new object[] { LogLevel.Warning, Properties.Resources.WARNING_UpdateManager });
            }
        }
    }
}
