﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LAeMail.Infrastructure.Controls
{
    /// <summary>
    /// Interaction logic for HtmlControl.xaml
    /// </summary>
    public partial class HtmlControl : UserControl
    {
        public HtmlControl()
        {
            InitializeComponent();

            CommandBindings.Add(new CommandBinding(
                NavigationCommands.BrowseBack,
                (object sender, ExecutedRoutedEventArgs e) => { wbBrowser.GoBack(); },
                (object s, CanExecuteRoutedEventArgs e) => { e.CanExecute = wbBrowser.CanGoBack; }
            )); 

            CommandBindings.Add(new CommandBinding(
                NavigationCommands.BrowseForward,
                (object sender, ExecutedRoutedEventArgs e) => { wbBrowser.GoForward(); }, 
                (object s, CanExecuteRoutedEventArgs e) => { e.CanExecute = wbBrowser.CanGoForward; }
            ));
            CommandBindings.Add(new CommandBinding(
                NavigationCommands.BrowseHome,
                (object s, ExecutedRoutedEventArgs e) => { DoBrowse(); }
            ));
        
            wbBrowser.Navigated += (object sender, NavigationEventArgs e) => { wbBrowser.Focus(); };
        }

        #region HtmlText
        public static readonly DependencyProperty HtmlTextProperty = DependencyProperty.Register("HtmlText", typeof(string), typeof(HtmlControl));

        public string HtmlText
        {
            get { return (string)GetValue(HtmlTextProperty); }
            set { SetValue(HtmlTextProperty, value); }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == HtmlTextProperty)
            {
                DoBrowse();
            }
        }

        private void DoBrowse()
        {
            if (!string.IsNullOrEmpty(HtmlText))
            {
                wbBrowser.Source = new Uri(HtmlText);
            }
        }
        #endregion
    }
}
