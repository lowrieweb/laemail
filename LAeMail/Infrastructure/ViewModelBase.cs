﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace LAeMail.Infrastructure
{
    /// <summary>
    /// Base class for all ViewModel classes in the application.
    /// It provides support for property change notifications 
    /// and has a DisplayName property.  This class is abstract.
    /// </summary>
    public abstract class ViewModelBase : PropertyHelpersBase
    {
        #region DisplayName

        /// <summary>
        /// Returns the user-friendly name of this object.
        /// Child classes can set this property to a new value,
        /// or override it to determine the value on-demand.
        /// </summary>
        public string DisplayName
        {
            get { return Get(() => DisplayName); }
            set { Set(() => DisplayName, value); }
        }
        #endregion // DisplayName
    }
}