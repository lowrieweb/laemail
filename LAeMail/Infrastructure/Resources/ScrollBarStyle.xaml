﻿<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml">

    <Style x:Key="ScrollBarLineButton" TargetType="{x:Type RepeatButton}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="OverridesDefaultStyle" Value="true" />
        <Setter Property="Focusable" Value="false" />
        <Setter Property="Background" Value="{StaticResource ScrollBarNormalBrush}" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type RepeatButton}">
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsMouseOver" Value="True">
                            <Setter Property="Background" Value="{StaticResource ScrollBarHighlightBrush}" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                    <Border Background="White">
                        <Path x:Name="Arrow" HorizontalAlignment="Center" VerticalAlignment="Center" Fill="{TemplateBinding Background}" Data="{Binding Content, RelativeSource={RelativeSource TemplatedParent}}" />
                    </Border>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <Style x:Key="VerticalScrollBarPageButton" TargetType="{x:Type RepeatButton}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="OverridesDefaultStyle" Value="true" />
        <Setter Property="IsTabStop" Value="false" />
        <Setter Property="Focusable" Value="false" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type RepeatButton}">
                    <Border Background="White">
                        <Rectangle Width="1" Fill="LightGray" />
                    </Border>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <Style x:Key="HorizontalScrollBarPageButton" TargetType="{x:Type RepeatButton}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="OverridesDefaultStyle" Value="true" />
        <Setter Property="IsTabStop" Value="false" />
        <Setter Property="Focusable" Value="false" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type RepeatButton}">
                    <Border Background="White">
                        <Rectangle Height="1" Fill="LightGray" />
                    </Border>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>
    
    <Style x:Key="ScrollBarThumb" TargetType="{x:Type Thumb}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="OverridesDefaultStyle" Value="true" />
        <Setter Property="IsTabStop" Value="false" />
        <Setter Property="Focusable" Value="false" />
        <Setter Property="Background" Value="{StaticResource ScrollBarNormalBrush}" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type Thumb}">
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsMouseOver" Value="True">
                            <Setter Property="Background" Value="{StaticResource ScrollBarHighlightBrush}" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                    <Rectangle Fill="{TemplateBinding Background}" />
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <ControlTemplate x:Key="VerticalScrollBar" TargetType="{x:Type ScrollBar}">
        <Grid>
            <Grid.RowDefinitions>
                <RowDefinition MaxHeight="16" />
                <RowDefinition Height="*" />
                <RowDefinition MaxHeight="16" />
            </Grid.RowDefinitions>
            <RepeatButton Style="{StaticResource ScrollBarLineButton}" Height="16" Command="ScrollBar.LineUpCommand" Content="M 0 12 L 3 0 L 6 12 Z" />
            <Track x:Name="PART_Track" Grid.Row="1" IsDirectionReversed="true">
                <Track.DecreaseRepeatButton>
                    <RepeatButton Style="{StaticResource VerticalScrollBarPageButton}" Command="ScrollBar.PageUpCommand" />
                </Track.DecreaseRepeatButton>
                <Track.Thumb>
                    <Thumb Style="{StaticResource ScrollBarThumb}" />
                </Track.Thumb>
                <Track.IncreaseRepeatButton>
                    <RepeatButton Style="{StaticResource VerticalScrollBarPageButton}" Command="ScrollBar.PageDownCommand" />
                </Track.IncreaseRepeatButton>
            </Track>
            <RepeatButton Grid.Row="2" Style="{StaticResource ScrollBarLineButton}" Height="16" Command="ScrollBar.LineDownCommand" Content="M 0 0 L 3 12 L 6 0 Z" />
        </Grid>
    </ControlTemplate>

    <ControlTemplate x:Key="HorizontalScrollBar" TargetType="{x:Type ScrollBar}">
        <Grid>
            <Grid.ColumnDefinitions>
                <ColumnDefinition MaxWidth="16" />
                <ColumnDefinition Width="*" />
                <ColumnDefinition MaxWidth="16" />
            </Grid.ColumnDefinitions>
            <RepeatButton Grid.Column="0" Style="{StaticResource ScrollBarLineButton}" Width="16" Command="ScrollBar.LineLeftCommand" Content="M 12 0 L 0 3 L 12 6 Z" />
            <Track x:Name="PART_Track" Grid.Column="1" IsDirectionReversed="False">
                <Track.DecreaseRepeatButton>
                    <RepeatButton Style="{StaticResource HorizontalScrollBarPageButton}" Command="ScrollBar.PageLeftCommand" />
                </Track.DecreaseRepeatButton>
                <Track.Thumb>
                    <Thumb Style="{StaticResource ScrollBarThumb}" />
                </Track.Thumb>
                <Track.IncreaseRepeatButton>
                    <RepeatButton Style="{StaticResource HorizontalScrollBarPageButton}" Command="ScrollBar.PageRightCommand" />
                </Track.IncreaseRepeatButton>
            </Track>
            <RepeatButton Grid.Column="3" Style="{StaticResource ScrollBarLineButton}" Width="16" Command="ScrollBar.LineRightCommand" Content="M 0 0 L 12 3 L 0 6 Z" />
        </Grid>
    </ControlTemplate>

    <Style x:Key="{x:Type ScrollBar}"
       TargetType="{x:Type ScrollBar}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="OverridesDefaultStyle" Value="true" />
        <Style.Triggers>
            <Trigger Property="Orientation" Value="Horizontal">
                <Setter Property="Width" Value="Auto" />
                <Setter Property="Height" Value="6" />
                <Setter Property="Margin" Value="0,5,0,5" />
                <Setter Property="Template" Value="{StaticResource HorizontalScrollBar}" />
            </Trigger>
            <Trigger Property="Orientation" Value="Vertical">
                <Setter Property="Width" Value="6" />
                <Setter Property="Height" Value="Auto" />
                <Setter Property="Margin" Value="5,0,5,0" />
                <Setter Property="Template" Value="{StaticResource VerticalScrollBar}" />
            </Trigger>
        </Style.Triggers>
    </Style>

</ResourceDictionary>